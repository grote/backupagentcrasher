package de.grobox.backupagentcrasher

import android.app.Activity
import android.content.ComponentName
import android.content.Intent
import android.util.Log
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT

class MainActivity : Activity() {

    override fun onStart() {
        super.onStart()
        Toast.makeText(this, "Out of FORCE_STOPPED!", LENGTH_SHORT).show()
        val i = Intent().apply {
            component = ComponentName.createRelative(
                "com.stevesoltys.seedvault",
                "com.stevesoltys.seedvault.settings.SettingsActivity",
            )
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }
        try {
            startActivity(i)
            finish()
        } catch (e: Exception) {
            Log.e("BackupAgentCrasher", "Unable to start Seedvault: ", e)
        }
    }
}
